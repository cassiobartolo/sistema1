from django import forms
from .models import clientes

class InsereClienteForm(forms.ModelForm):
  class Meta:
    # Modelo base
    model = clientes

    # Campos que estarão no form
    fields = [
     'nome',
      'cpf',
      'rg',
      'email',
      'nome_mae',
      'telefone',
      'rua',
      'bairro',
      'numero',
      'cidade',
      'estado',
      'estado_civil',
      'situacao'
    ]

    widgets = {
      "nome": forms.TextInput(attrs={"class": "form-control"}),
      "cpf": forms.TextInput(attrs={"class": "form-control"}),
      "rg": forms.TextInput(attrs={"class": "form-control"}),
      "email": forms.TextInput(attrs={"class": "form-control"}),
      "nome_mae": forms.TextInput(attrs={"class": "form-control"}),
      "telefone": forms.TextInput(attrs={"class": "form-control"}),
      "rua": forms.TextInput(attrs={"class": "form-control"}),
      "bairro": forms.TextInput(attrs={"class": "form-control"}),
      "numero": forms.TextInput(attrs={"class": "form-control"}),
      "estado": forms.TextInput(attrs={"class": "form-control"}),
      "cidade": forms.TextInput(attrs={"class": "form-control"}),
      "estado_civil": forms.TextInput(attrs={"class": "form-control"}),
      "situacao": forms.Select(attrs={"class": "form-control"}),
    }