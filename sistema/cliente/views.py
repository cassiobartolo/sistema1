from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.views.generic import CreateView, ListView, UpdateView, FormView, DetailView, TemplateView
from django.utils import timezone

from csv_export.views import CSVExportView

from .forms import InsereClienteForm
from .models import clientes


class IndexView(TemplateView):
  template_name = "index.html"

class ClienteCreateView(CreateView):
  template_name = "cadastrar.html"
  model = clientes
  form_class = InsereClienteForm
  success_url = reverse_lazy("cliente:index")


class DataExportView(CSVExportView):
  model = clientes
  fields = ('nome','cpf', 'email', 'telefone', 'situacao')
  header = False
  specify_separator = False
 

  def get_filename(self, queryset):
    return 'importa.csv'


class ListaListView(TemplateView):
  template_name = "lista.html"

  def get_context_data(self, **kwargs):
    context = super().get_context_data(**kwargs)
    context["Clientes"] = clientes.objects.all()
    return context


class EditaClienteView(UpdateView):
  template_name = "cadastrar.html"
  model = clientes
  form_class = InsereClienteForm
  success_url = reverse_lazy("cliente:index")

  def get(self, request, *args, **kwargs):
      self.object = self.get_object()
      form = self.form_class(instance=self.object)
      return self.render_to_response(self.get_context_data(form=form))

  def post(self, request, *args, **kwargs):
      self.object = self.get_object()
      form_class = self.get_form_class()
      form = form_class(request.POST, instance=self.object)
      if form.is_valid():
          self.object = form.save()
          return self.form_valid(form)
      return self.form_invalid(form)