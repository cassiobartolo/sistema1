from django.db import models
from django.conf import settings
from  django.utils.timezone import timezone

class clientes(models.Model):

  STATUS = (
    (0, ("Inativo")), 
    (1, ("Ativo"))
  )

  nome = models.CharField(
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Nome"
  )
  
  email = models.EmailField(
    default=' ',
    unique=True,
    max_length=255,
    null=False,
    blank=False,
    verbose_name="E-mail"
  )

  cpf = models.CharField(
    unique=True,
    max_length=11,
    null=False,
    blank=False,
    verbose_name="CPF"
  )
  
  nome_mae = models.CharField(
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Nome Mãe"
  )

  rg = models.CharField(
    max_length=11,
    null=False,
    blank=False,
    verbose_name="RG"
  )

  telefone = models.CharField(
    max_length=11,
    null=False,
    blank=False,
    verbose_name="Telefone"
  )
  
  rua = models.CharField(
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Rua"
  )
  
  bairro = models.CharField(
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Bairro"
  )

  cidade = models.CharField(
    default=' ',
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Cidade"
  )

  estado = models.CharField(
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Estado"
  )
  
  numero = models.IntegerField(
    null=False,
    blank=False,
    verbose_name="Número Residencia"
  )

  estado_civil = models.CharField(
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Estado Civil"
  )

  situacao = models.IntegerField(
    choices=STATUS, 
    default=1, 
    verbose_name="Situação"
  )

  class Meta:
    verbose_name = "clientes"
    verbose_name_plural = "cliente"
    
  def __str__(self):
    return self.nome